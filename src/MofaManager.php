<?php declare(strict_types=1);
/**
 * Shopware
 * Copyright © 2022
 *
 * @category   Shopware
 * @package    sw_6416dev
 * @subpackage Manager.php
 *
 * @copyright  2022 Iguana-Labs GmbH
 * @author     Module Factory <info at module-factory.com>
 * @license    https://www.module-factory.com/eula
 */

namespace Mofa\Manager;

use Shopware\Administration\DependencyInjection\AdministrationMigrationCompilerPass;
use Shopware\Core\Framework\Bundle;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;

class MofaManager extends Bundle
{
    public function getTemplatePriority(): int
    {
        return -1;
    }

    public function build(ContainerBuilder $container): void
    {
        parent::build($container);

        $loader = new XmlFileLoader($container, new FileLocator(__DIR__ . '/Resources/config/'));
        $loader->load('services.xml');

        $container->addCompilerPass(new AdministrationMigrationCompilerPass());
    }
}
