### MofaManager [![Build Status](https://img.shields.io/badge/build-passing-brightgreen.svg)]()



### Getting started

- under ShopwareRoot/composer.json add the following:
```json
{
    "require": {
      "mofa/manager": "dev-main"
    },
    "repositories": [
        {
            "type": "vcs", 
            "url": "https://gitlab.com/GeroBittner/mofa-manager.git"
        }
    ],
}
```
- under ShopwareRoot/config/bundles.php add the following:
```php
<?php

return [
    Mofa\Manager\MofaManager::class => ['all' => true],
];
```
